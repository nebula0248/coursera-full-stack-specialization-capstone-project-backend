module.exports = {
    error: function(res, errorMessage) {
        res.status(500).json({
            "error": errorMessage
        });
    },

    data: function(res, returningData) {
        res.json(returningData);
    },

    message: function(res, message) {
        res.json({
            "message": message
        });
    }
};