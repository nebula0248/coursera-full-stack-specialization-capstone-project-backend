var express = require('express');
var router = express.Router();
var fs = require('fs');

var multer  = require('multer');
var upload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            var dest = './public/uploads/';
            if (!fs.existsSync(dest)){
                fs.mkdirSync(dest);
            }
            cb(null, dest);
        },
        filename: function (req, file, cb) {
            cb(null, Math.ceil(Math.random() * 100000000) + file.originalname);
        }
    })
});

var ResponseHelper = require('../helpers/response-helper');
var Book = require('../models/books');

router.get('/', function(req, res, next) {
    res.render('index');
});

router.get('/books/all', function(req, res, next) {
    Book.find({}, function(err, books) {
        if(err) {
            ResponseHelper.error(res, "Cannot retrieve all book data.");
            return;
        }

        var hostURL = (req.secure ? "https://" : "http://") + req.headers.host;
        books.forEach(function(theBook, index) {
            theBook.image = hostURL + "/uploads/" + theBook.image;
        });
        ResponseHelper.data(res, books);
    });
});

router.get('/books/search/title/:searchingTitle', function(req, res, next) {
    var searchingTitle = req.params.searchingTitle;

    Book.find({
        "title": {"$regex": searchingTitle, "$options": "i" }
    }, function(err, books) {
        if(err) {
            ResponseHelper.error(res, "Cannot search for book data.");
            return;
        }

        var hostURL = (req.secure ? "https://" : "http://") + req.headers.host;
        books.forEach(function(theBook, index) {
            theBook.image = hostURL + "/uploads/" + theBook.image;
        });
        ResponseHelper.data(res, books);
    });
});

router.get('/books/get/:id', function(req, res, next) {
    var requestBookId = req.params.id;

    Book.find({
        "_id": requestBookId
    }, function(err, books) {
        if(err) {
            ResponseHelper.error(res, "Cannot retrieve the book data.");
            return;
        }

        var hostURL = (req.secure ? "https://" : "http://") + req.headers.host;
        books.forEach(function(theBook, index) {
            theBook.image = hostURL + "/uploads/" + theBook.image;
        });
        ResponseHelper.data(res, books[0]);
    });
});

router.post('/books/create', upload.single('image'), function(req, res, next) {
    var newBook = new Book({
        title: req.body.title,
        author: req.body.author,
        description: req.body.description,
        price: req.body.price,
        image: req.file ? req.file.filename : '',
        isbn: req.body.isbn,
        seller: req.body.seller,
        sellerContact: req.body.sellerContact
    });

    newBook.save(function(err) {
       if(err) {
           ResponseHelper.error(res, "Cannot create a new book record.");
           return;
       }
       ResponseHelper.message(res, "A new book is created.");
    });
});

router.post('/books/comments/create/:bookId', function(req, res, next) {
    var requestBookId = req.params.bookId;

    Book.find({
        "_id": requestBookId
    }, function(err, books) {
        if(err || books.length <= 0) {
            ResponseHelper.error(res, "Cannot retrieve the book data.");
            return;
        }

        var theBook = books[0];
        theBook.comments.push({
            commenter: req.body.commenter,
            content: req.body.content
        });

        theBook.save(function(err) {
            if(err) {
                ResponseHelper.error(res, "Cannot save the comment.");
                return;
            }
            ResponseHelper.message(res, "A new comment is created.");
        });
    });
});

module.exports = router;
