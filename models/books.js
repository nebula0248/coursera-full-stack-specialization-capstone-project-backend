var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Book', new Schema({
        title: {
            type: String,
            required: true
        },
        author: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        image: {
            type: String,
            required: false,
            default: ''
        },
        price: {
            type: Number,
            required: true
        },
        isbn: {
            type: String,
            required: true
        },
        seller: {
            type: String,
            required: true
        },
        sellerContact: {
            type: String,
            required: true
        },
        comments: [new Schema({
            commenter: {
                type: String,
                required: true
            },
            content: {
                type: String,
                required: true
            }
        })]
    })
);